
    Integrate w3data.js @ http://www.w3schools.com/w3css/w3data_home.asp

    Use it to load pages.
        - Format:
            {
                title: "yeah",
                description: "yeah yeah",
                directory: "/1-yeah-html/"
            }
        - In each directory
            - page.txt
            - other files, such as gif/png/js etc etc.
            - may also include subfolders for examples

    https://github.com/mrdoob/stats.js



    Using Texture Packer w/ PIXI.

    PIXI Animated Sprites: Using Texture.fromImage

    PIXI Animated Sprites: Using 1 sheet per unit animation.

    PIXI Animated Sprites: Using 1 sheet per unit

    PIXI Animated Sprites: Using 1 sheet for multiple units & animations

    Animated Sprites w/ PIXI v4
        1. Basic usage of PIXI AnimatedSprite
            - You basically put all images to an array,
            - Then you do Texture.fromImage
            - Then create
            - http://stackoverflow.com/questions/41443316/pixijs-v4-limiting-frame-rate
        2. Dealing with Multiple Animations.
            - Use of array.
            - Efficient loading & referencing.